## About

Custom CSS code for GUADEC's old WordPress website. This was defined 
under Appearance -> Custom CSS.

The source code for the child theme is located at:
https://gitlab.gnome.org/Infrastructure/guadec-web/tree/guadec2017

(the parent theme is Sensible WP)

## Contribute

To make a contribution to GUADEC's theme see: https://gitlab.gnome.org/Infrastructure/guadec-web/tree/master/


